from dataclasses import dataclass


@dataclass()
class rodict:
    """
    Read only dict, supports hierarchies.
    """
    _dict: dict

    def __init__(self, d):
        self._dict= d

    def __len__(self):
        return len(self._dict)

    def __iter__(self):
        return iter(self._dict)

    def _wrap(self, item):
        return type(self)(item) if isinstance(item, type(self._dict)) else item

    def __getitem__(self, key):
        return self._wrap(item= self._dict.__getitem__(key))

    def __getattr__(self, key):
        return self._wrap(item= getattr(self._dict, key))

    def __setattr__(self, key, value):
        if key.startswith('_'):
            super().__setattr__(key, value)
        else:
            raise TypeError
