from dataclasses import dataclass
from xdict import *

import logging
logger= logging.getLogger(__name__)


class hdict(xdict):
    """
    Hierarchical (extended) dict: special operations for the case when hdicts appear as members of hdicts.

    Features:
    - item access by path: d['a.b.c'] is equivalent to d['a']['b']['c'] and to d.a.b.c
    - get, set, del operations are all supported
    """

    _separator= '.'

    def _split_path(self, path):
        prefix, _, key = path.rpartition(self._separator)
        return prefix, key

    def _getpath(self, key):
        if isinstance(key, str):
            prefix, key = self._split_path(path= key)
        else:
            prefix= ''

        item = self[prefix] if prefix else self
        return bool(prefix), item, key

    def __getitem__(self, key):
        path, item, key = self._getpath(key= key)
        if not path:
            value= super().__getitem__(key)
        else:
            value= item[key]

        return value

    def __setitem__(self, key, value):
        path, item, key = self._getpath(key= key)
        if not path:
            super().__setitem__(key, value)
        else:
            item[key]= value

    def __delitem__(self, key):
        path, item, key = self._getpath(key= key)
        if not path:
            super().__delitem__(key)
        else:
            del item[key]

    def _traversal(self, node_callback= None, leaf_callback= None):
        def rec_traversal(path, node):
            if node_callback:
                node_callback(path= path, node= node)
            for sub in node:
                subpath= path + self._separator + sub
                if isinstance(node[sub], hdict):
                    rec_traversal(path= subpath, node= node[sub])
                elif leaf_callback:
                    node[sub]= leaf_callback(path= subpath, node= node[sub])

        rec_traversal(path= '', node= self)

    def _leaves(self):
        leaves= []
        def leaf_collector(path, node):
            nonlocal leaves
            leaves.append(node)
            return node

        self._traversal(leaf_callback= leaf_collector)

        return leaves



