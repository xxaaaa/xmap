from idict import idict
from attr2item import attr2item

import logging
logger= logging.getLogger(__name__)


class xdict(idict,attr2item):
    """
    Extended dict with attribute access to values.

    """

    class selector(idict.selector):
        def __getattr__(self, key):
            return self[key]

    def _validkey(self, key):
        return super()._validkey(key= key) and not isinstance(self._ikey(key= key), int)

    def _check_validity(self):
        if not all(self._validkey(key) for key in self):
            raise KeyError

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._check_validity()


