

class attr2item:
    """
    Mixin class to translate attribute access to item access.
    `d.a` is equivalent to `d[a]`
    `d.a= e` is equivalent to `d[a]= e`
    `del d.a` is equivalent to `del d[a]`
    """
    def _validkey(self, key):
        """Check if key is valid for translation."""
        return not key.startswith('_')

    def __getattr__(self, key):
        return self.__getitem__(key)

    def __setattr__(self, key, value):
        if not self._validkey(key= key):
            super().__setattr__(key, value)
        else:
            self.__setitem__(key, value)

    def __delattr__(self, key):
        if not self._validkey(key= key):
            super().__delattr__(key)
        else:
            self.__delitem__(key)


