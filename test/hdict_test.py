import unittest
from hdict import *


class hdictTest(unittest.TestCase):
    def test_ctor_default(self):
        d= hdict()

        self.assertEqual(0, len(d))
        with self.assertRaises(KeyError):
            d['a']

        with self.assertRaises(IndexError):
            d[0]

    def test_ctor_init_flat(self):
        d= hdict(a= 1, b= 2, c= 3)

        self.assertEqual(3, len(d))
        self.assertEqual(1, d['a'])
        self.assertEqual(2, d['b'])
        self.assertEqual(3, d['c'])

    def test_ctor_init_hierarchy(self):
        d= hdict(a= 1, b= 2, c= hdict(x= 7, y= 8))

        self.assertEqual(3, len(d))
        self.assertEqual(7, d.c.x)
        self.assertEqual(7, d['c.x'])
        self.assertEqual(8, d['c.y'])

        with self.assertRaises(KeyError):
            d['c.z']

    def test_setitem(self):
        d= hdict()

        self.assertEqual(0, len(d))

        d['a']= 1
        self.assertEqual(1, len(d))
        self.assertEqual(1, d.a)

        with self.assertRaises(KeyError):
            d['b.x']= 7

        self.assertEqual(1, len(d))

        d.b= hdict(x= 7, y= 8)
        self.assertEqual(2, len(d))
        d['b.z']= 99

        self.assertEqual(2, len(d))
        self.assertEqual(3, len(d.b))
        self.assertEqual(99, d.b.z)
        self.assertEqual(99, d['b.z'])

        d['b.z']= 100
        self.assertEqual(2, len(d))
        self.assertEqual(3, len(d.b))
        self.assertEqual(100, d.b.z)
        self.assertEqual(100, d['b.z'])

    def test_delitem(self):
        d= hdict(a= 1, b= 2, c= hdict(x= 7, y= 8))

        del d['c.x']
        self.assertEqual(1, len(d.c))
        with self.assertRaises(KeyError):
            d.c.x

    def test_iteration(self):
        d= hdict(a= 1, b= 2, c= 3)

        self.assertEqual(['a', 'b', 'c'], [x for x in d])
        self.assertEqual(['a', 'b', 'c'], [x for x in d.keys()])
        self.assertEqual([1, 2, 3], [x for x in d.values()])
        self.assertEqual([('a', 1), ('b', 2), ('c', 3)], [x for x in d.items()])

    def test_traversal(self):
        count= 0
        def callback(path, node):
            nonlocal count
            count += node

            return node

        d = hdict(a=1, b=2, c=hdict(x=7, y=8))
        d._traversal(leaf_callback= callback)

        self.assertEqual(18, count)

    def test_leaves(self):
        d= hdict(a=1, b=2, c=hdict(x=7, y=8))
        leaves= d._leaves()
        self.assertEqual([1, 2, 7, 8], leaves)


if __name__ == '__main__':
    unittest.main()
