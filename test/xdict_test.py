import unittest
from xdict import *


class XDictTest(unittest.TestCase):
    def test_ctor_default(self):
        d= xdict()

        self.assertEqual(0, len(d))
        with self.assertRaises(KeyError):
            d.a
        with self.assertRaises(KeyError):
            d['a']

        self.assertEqual(0, len(d))
        d._a= 1
        self.assertEqual(0, len(d))
        self.assertEqual(1, d._a)

    def test_ctor_init(self):
        with self.assertRaises(KeyError):
            d= xdict(_a= 1)

        d= xdict(a= 1, b= 2, c= 3)
        self.assertEqual(3, len(d))

        self.assertEqual(1, d.a)
        self.assertEqual(1, d['a'])
        self.assertEqual(1, d[0])

        self.assertEqual(2, d.b)
        self.assertEqual(2, d['b'])
        self.assertEqual(2, d[1])

        self.assertEqual(3, d.c)
        self.assertEqual(3, d['c'])
        self.assertEqual(3, d[2])

        with self.assertRaises(KeyError):
            d.z

        with self.assertRaises(KeyError):
            d['z']

        with self.assertRaises(IndexError):
            d[3]

        self.assertEqual(3, d[-1])
        self.assertEqual(2, d[-2])
        self.assertEqual(1, d[-3])

    def test_ikey(self):
        d = xdict(a=1, b=2, c=3)

        self.assertEqual('a', d.ikey(0))
        self.assertEqual('b', d.ikey(1))
        self.assertEqual('c', d.ikey(2))

    def test_setitem(self):
        d= xdict()

        d.a= 1
        self.assertEqual(1, d.a)
        self.assertEqual(1, d['a'])
        self.assertEqual(1, d[0])

        self.assertEqual(1, len(d))

        d['b']= 2
        self.assertEqual(2, d.b)
        self.assertEqual(2, d['b'])
        self.assertEqual(2, d[1])

        self.assertEqual(2, len(d))

        d[0]= 9
        self.assertEqual(9, d.a)
        self.assertEqual(9, d['a'])
        self.assertEqual(9, d[0])

        self.assertEqual(2, len(d))

    def test_delitem(self):
        d= xdict(a= 1, b= 2, c= 3, z= 99)

        del d.a
        self.assertEqual(3, len(d))
        with self.assertRaises(KeyError):
            d.a
        self.assertEqual(2, d[0])
        self.assertEqual(3, d[1])
        self.assertEqual(99, d[2])

        del d['b']
        self.assertEqual(2, len(d))
        with self.assertRaises(KeyError):
            d['b']
        self.assertEqual(3, d[0])
        self.assertEqual(99, d[1])

        del d[0]
        self.assertEqual(1, len(d))
        with self.assertRaises(KeyError):
            d.c
        self.assertEqual(99, d[0])

        del d[0]
        self.assertEqual(0, len(d))

    def test_iteration(self):
        d= xdict(a= 1, b= 2, c= 3)

        self.assertEqual(['a', 'b', 'c'], [x for x in d])
        self.assertEqual(['a', 'b', 'c'], [x for x in d.keys()])
        self.assertEqual([1, 2, 3], [x for x in d.values()])
        self.assertEqual([('a', 1), ('b', 2), ('c', 3)], [x for x in d.items()])

    def test_members(self):
        d= xdict(a=1, b=2, c=3)

        self.assertTrue('a' in d)
        self.assertFalse('z' in d)

    def test_append(self):
        d= xdict(a= 1, b= 2)
        d.append()

        self.assertEqual(2, len(d))

        d.append(3, 4)
        self.assertEqual(4, len(d))
        self.assertEqual(3, d[2])
        self.assertEqual(4, d[3])

    def test_subcomp(self):
        d= xdict(xdict(x=1,y=2),a=xdict(x=3,y=4),b=xdict(x=5,y=6))
        s=d[...].x

        self.assertEqual(3, len(s))
        self.assertEqual(1, s[0])
        self.assertEqual(3, s[1])
        self.assertEqual(5, s[2])

        s=d[...].y

        self.assertEqual(3, len(s))
        self.assertEqual(2, s[0])
        self.assertEqual(4, s[1])
        self.assertEqual(6, s[2])




if __name__ == '__main__':
    unittest.main()
