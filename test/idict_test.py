import unittest
from idict import idict


class idictTest(unittest.TestCase):
    def _test_ctor(self, d):
        self.assertEqual(3, len(d))

        self.assertEqual(1, d[0])
        self.assertEqual(1, d['0'])
        self.assertEqual(2, d[1])
        self.assertEqual(2, d['1'])
        self.assertEqual(3, d[2])
        self.assertEqual(3, d['2'])

        with self.assertRaises(IndexError):
            d[3]
        with self.assertRaises(IndexError):
            d['3']

        self.assertEqual(3, d[-1])
        self.assertEqual(3, d['-1'])
        self.assertEqual(2, d[-2])
        self.assertEqual(2, d['-2'])
        self.assertEqual(1, d[-3])
        self.assertEqual(1, d['-3'])

        with self.assertRaises(IndexError):
            d[-4]
        with self.assertRaises(IndexError):
            d['-4']

    def test_ctor_empty(self):
        d= idict()
        self.assertEqual(0, len(d))
        self.assertEqual('', d._content_str())

    def test_ctor_args(self):
        d= idict(1, 2, 3)
        self._test_ctor(d)

        self.assertEqual('key0', d.ikey(0))
        self.assertEqual('key1', d.ikey(1))
        self.assertEqual('key2', d.ikey(2))

    def test_ctor_kwargs(self):
        d= idict(a= 1, b= 2, c= 3)
        self._test_ctor(d)

        self.assertEqual('a', d.ikey(0))
        self.assertEqual('b', d.ikey(1))
        self.assertEqual('c', d.ikey(2))

    def test_ctor_mixed(self):
        d= idict(1, 2, c= 3)
        self._test_ctor(d)

        self.assertEqual('key0', d.ikey(0))
        self.assertEqual('key1', d.ikey(1))
        self.assertEqual('c', d.ikey(2))

    def test_ctor_update(self):
        d= idict(1, 2, 4, key2= 3)
        self._test_ctor(d)

        self.assertEqual('key0', d.ikey(0))
        self.assertEqual('key1', d.ikey(1))
        self.assertEqual('key2', d.ikey(2))

    def test_fromvalues(self):
        d= idict.fromvalues(1, 2, 3)
        self._test_ctor(d)

        self.assertEqual('key0', d.ikey(0))
        self.assertEqual('key1', d.ikey(1))
        self.assertEqual('key2', d.ikey(2))

    def test_clone(self):
        d= idict(a= 1, b= 2, c= [3])
        x= d._clone()

        self.assertEqual(3, len(x))
        self.assertEqual(1, x[0])
        self.assertEqual(2, x[1])
        self.assertEqual([3], x[2])

        d[2].append(4)
        self.assertEqual([3, 4], d[2])
        self.assertEqual([3], x[2])

    def test_slice(self):
        d= idict(a= 1, b= 2, c= 3)
        x= d[:]
        self.assertEqual(d, x)

        x= d[2:3]
        self.assertEqual(1, len(x))
        self.assertEqual(3, x['c'])
        self.assertEqual(3, x[0])

        x= d[:-1]
        self.assertEqual(2, len(x))
        self.assertEqual(2, x['b'])
        self.assertEqual(2, x[1])

    def test_setitem(self):
        d= idict(a= 1, b=2, c= 3)

        d[0]= 7
        self.assertEqual(7, d['a'])

        d[1]= 8
        self.assertEqual(8, d['b'])

        d[-1]= 9
        self.assertEqual(9, d['c'])

        with self.assertRaises(IndexError):
            d[3]

    def test_delitem(self):
        d= idict(a= 1, b= 2, c= 3)
        del d['a']
        self.assertEqual(2, len(d))

        del d[0]
        self.assertEqual(1, len(d))
        self.assertEqual(3, d[0])
        self.assertEqual('c', d.ikey(0))

    def test_append(self):
        d= idict()
        d.append(1, 2, 3)

        self._test_ctor(d)
        self.assertEqual('key0', d.ikey(0))
        self.assertEqual('key1', d.ikey(1))
        self.assertEqual('key2', d.ikey(2))

    def test_subcomp(self):
        d= idict(idict(x=1,y=2),a=idict(x=3,y=4),b=idict(x=5,y=6))
        s=d[...]['x']

        self.assertEqual(3, len(s))
        self.assertEqual(1, s[0])
        self.assertEqual(3, s[1])
        self.assertEqual(5, s[2])

        s=d[...][1]

        self.assertEqual(3, len(s))
        self.assertEqual(2, s[0])
        self.assertEqual(4, s[1])
        self.assertEqual(6, s[2])


if __name__ == '__main__':
    unittest.main()
