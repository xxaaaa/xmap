import unittest
from hdict import *
from rodict import *


class rodictTest(unittest.TestCase):
    def test_iteration(self):
        d= rodict(dict(a= 1, b= 2, c= 3))

        self.assertEqual(['a', 'b', 'c'], [x for x in d])
        self.assertEqual(['a', 'b', 'c'], [x for x in d.keys()])
        self.assertEqual([1, 2, 3], [x for x in d.values()])
        self.assertEqual([('a', 1), ('b', 2), ('c', 3)], [x for x in d.items()])

    def test_access(self):
        d= hdict(a=1, b=2, c=hdict(x=7, y=8))
        r= rodict(d)

        self.assertEqual(3, len(r))
        self.assertEqual(1, r.a)
        self.assertEqual(2, r.b)
        self.assertEqual(7, r.c.x)
        self.assertEqual(8, r.c.y)
        self.assertEqual(8, r['c.y'])

        with self.assertRaises(KeyError):
            r.z

        with self.assertRaises(TypeError):
            r.a= 99

        with self.assertRaises(TypeError):
            r.z= 99

    def test_extra_attrib(self):
        d=hdict()
        r= rodict(d)

        self.assertEqual(0, len(r))
        r._x= 1
        self.assertEqual(1, r._x)
        self.assertEqual(0, len(r))

        d._x= 2
        self.assertEqual(1, r._x)
        self.assertEqual(0, len(r))

        del r._x
        self.assertEqual(2, r._x)

        with self.assertRaises(AttributeError):
            del r._x


if __name__ == '__main__':
    unittest.main()
