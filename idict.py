import random
import copy
import math
import types

import logging
logger= logging.getLogger(__name__)


def stdstr(obj, details):
    return '{}({})'.format(obj.__class__.__name__, details)


class idict(dict):
    """
    idict: indexed dictionary
    Features:
    - list functionality: integers are not keys, but indices to values
        d[i] is the value of the i-th item in the dictionary
        numeric strings are also interpreted as indexes: d['0'] is equivalent to d[0]
    - append operation to add multiple new values with generated unique keys
    - subdict comprehension: d[...][k]
    """

    def _content_str(self):
        width   = 1 + int(math.log10(len(self))) if len(self) else 0
        fmt     = '{key}[{index:0{width}}]:{value}'
        content = [fmt.format(index= index, width= width, key= item[0], value= item[1]) for index, item  in enumerate(self.items())]
        return ', '.join(content)

    def __str__(self):
        return stdstr(self, self._content_str())

    def __repr__(self):
        return str(self)

    def _ikey(self, key):
        try:
            key= int(key)
        except (ValueError, TypeError):
            pass

        return key

    def ikey(self, key):
        key= self._ikey(key= key)
        return list(self)[key] if isinstance(key, int) else key

    def __init__(self, *args, **kwargs):
        super().__init__()
        self.append(*args)
        self.update(**kwargs)

    @classmethod
    def fromvalues(cls, *args):
        return cls(*args)

    def _empty_clone(self):
        return type(self)()

    def _subdict(self, items):
        d= self._empty_clone()
        d.update(**items)
        return d

    def __deepcopy__(self, memodict= {}):
        return self._subdict(items= {k:copy.deepcopy(v, memo= memodict) for k, v in self.items()})

    def _clone(self):
        return copy.deepcopy(self)

    def _shadow(self):
        return self._subdict(items= {k:v._shadow() for k, v in self.items()})

    class selector:
        def __init__(self, smap):
            self._smap = smap

        def __getitem__(self, key):
            a = self._smap._empty_clone()
            a.update(**{k: v[key] for k, v in self._smap.items()})
            return a

    def __getitem__(self, key):
        if isinstance(key, slice):
            return self._subdict(items= {self.ikey(key= k):self[k] for k in range(len(self))[key]})
        elif key is Ellipsis:
            return self.selector(smap= self)
        # elif isinstance(key, types.FunctionType):
        #     return self.selector(imap= self._subdict(items= {k:v for k,v in self.items() if key(k)}))
        else:
            return super().__getitem__(self.ikey(key= key))

    def __setitem__(self, key, value):
        super().__setitem__(self.ikey(key= key), value)

    def __delitem__(self, key):
        super().__delitem__(self.ikey(key= key))

    def _newkey(self, prefix= 'key'):
        unique, n = False, len(self)
        while not unique:
            key = '{}{}'.format(prefix, n)
            unique, n = key not in self, random.randint(0, 2 * len(self))
        return key

    def append(self, *args):
        for a in args:
            self[self._newkey()]= a
